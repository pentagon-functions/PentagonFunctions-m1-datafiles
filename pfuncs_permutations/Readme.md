Transformation of the one-mass pentagon functions under permutations of the external massless momenta.

**1m_pfuncs_sparse_pfuncs.m**: list of all pentagon functions.

**1m_pfuncs_sparse_monomials.m**: list of all pentagon function monomials appearing in the permutations of the pentagon functions.

**1m_pfuncs_sparse_**`sigma`**.m**: permutation `sigma` (e.g. `p13245`) of the pentagon functions represented as a sparse matrix. The transformation rules for the pentagon functions listed in **1m_pfuncs_sparse_pfuncs.m** are obtained by multiplying this matrix by the monomials in **1m_pfuncs_sparse_monomials.m**.

Example of usage in Mathematica:  

```
permutation = {3,2,4,5}; (* any permutation of {2,3,4,5} *)
pfuncs = Get["1m_pfuncs_sparse_funcs.m"]/.F->F[permutation];
pfuncmonomials = Get["1m_pfuncs_sparse_monomials.m"];
file = "1m_pfuncs_sparse_p1"<>StringJoin[ToString/@permutation]<>".m";
matrix = Normal[SparseArray[Get[file]]];
subFperm = Thread[pfuncs->matrix.pfuncmonomials];
```
