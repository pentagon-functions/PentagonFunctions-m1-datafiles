# PentagonFunctions M1 Datafiles
*Dmitry Chicherin*, *Vasily Sotnikov*, *Simone Zoia*

Data files for the planar one-mass pentagon functions from [arXiv:2110.10111].

### Notation

We parametrize the kinematics in terms of $`\{`$`p1s,s12,s23,s34,s45,s15`$`\} = \{p_1^2, s_{12}, s_{23}, s_{34}, s_{45}, s_{15}\}`$, with $`s_{ij} = (p_i+p_j)^2`$. Occasionally, we use non-adjacent scalar products `s[i,j]`$`=s_{ij}`$ to make the expressions more compact. We denote the square roots $`\text{tr}_5`$ and $`\sqrt{\Delta_3^{(i)}}`$ by `tr5` and `sqrtG3[i]`, respectively. We give explicit expressions for the square roots in **square_roots.m**.

We denote the pure master integrals by `mi[fam][sigma][i]`, where `fam`$`\in \{`$`1L`, `mzz`, `zmz`, `zzz`$`\}`$ is the name of the integral family, `sigma` is the permutation of the external massless momenta, and `i` is the position of the integral in the basis. The integral families are defined in section 3.1 of our paper [arXiv:2110.10111] and in the original work [arXiv:2005.04195]. The bases of pure integrals in the permutation $`\sigma_{\text{id}}=`$`{2,3,4,5}` of the external massless momenta are defined in [arXiv:2005.04195].

We denote by `{i2,i3,i4,i5}` the permutation of the external massless momenta $`\sigma=(i_2i_3i_4i_5)`$ such that $`\sigma(p_k) = p_{i_k}`$.

We denote the one-mass pentagon functions by `F[i,j]`$`=f^{(i)}_j`$, where `i`$`\in\{1,2,3,4\}`$ is the transcendental weight and `j` is a label.


### Files

**alphabet.m**: definition of the relevant letters `W[i]` of the one-mass pentagon alphabet defined in [arXiv:2107.14180].

**square_roots.m**: definition of the square roots in terms of independent Mandlestam invariants.

**alphabet_permutation_orbits.m**: relevant alphabet letters expressed as permutations of generating letters. The format is
`{W[i] -> obj, {i2,i3,i4,i5}}`, meaning that the letter `W[i]` is given by acting with the permutation `{2->i2,3->i3,4->i4,5->i5}` of the external massless momenta on the generating object `obj`.

**master_integral_mappings.m**: mappings among the pure master integrals in the form of substitution rules.

**mi2pfuncs.m**: expression of the independent pure master integrals in terms of one-mass pentagon functions `F[i,j]` and the transcendental constants `im[1,1]=I*Pi` and `re[3,1]=Zeta[3]`.

**pfuncs_charges.m**: charges of the one-mass pentagon functions under changing the signs of the square roots (see section 3.4 of [arXiv:2110.10111]). The file contains a list of replacement rules `F[i,j]->sqrt`, meaning that the function `F[i,j]` is odd with respect to changing the sign of the square root `sqrt`.

**transcendental_constants.m**: definition of the transcendental constants `im[1,1]`, `re[i,j]`, `tci[i,j]` and `tcr[i,j]` appearing in the expressions of the pentagon functions. The constants of weight 1 and 2 (and `re[3,1]`) are given explicitly in terms of `Pi`, `Zeta[3]`, logarithms and dilogathms. At weight 3 and 4 we give the constants in the form `const->{mi[fam,perm,w,i],value}`, which means that the constant `const` is given by the order-$`\epsilon^{\text{w}}`$ component of the `i`-th pure integral of the family `fam` in the permutation `perm` evaluated at the initial point $`X_0`$ (defined in section 3.5 of [arXiv:2110.10111]). For the constants `tcr[i,j]` (`tci[i,j]`) one must take the real (imaginary, including the factor of $`i`$) part. `value` then gives the numerical value of the constant `const` with 3000-digit accuracy.

**pfuncs_expressions.m**: expressions of the one-mass pentagon functions `F[i,j]` (see section 4.2 of [arXiv:2110.10111]). At transcendental weights 1 and 2 we give expressions in terms of logarithms `Log[x]` and dilogarithms `PolyLog[2,x]`. At transcendental weights 3 and 4 we express the functions as one-fond integrals. The integrands of the latter contain pentagon functions of weights 1 and 2, transcendental constants, `DLog[W[i]]`$`=\partial_t \log W_i(t)`$, and `IntDLog[W[i]]`$`=\int_s^1 dt \, \partial_t \log W_i(t)`$ ($`L_i(s)`$ in [arXiv:2110.10111]). See sections 4.2.3 and 4.2.4 of [arXiv:2110.10111] for a thorough discussion.

**pfuncs_iterated_integrals.m**: expressions of the one-mass pentagon functions `F[i,j]` in terms of Chen's iterated integrals and transcendental constants. By `ii[i1,i2,...,in]` we denote the iterated integral $`[W_{i_1},W_{i_2},\ldots,W_{i_n}]_{X_0}`$ (see section 3.4 of [arXiv:2110.10111]).

**pfuncs_permutations/**: contains the permutations of the pentagon functions. See the readme file there.



[arXiv:2110.10111]: https://arxiv.org/abs/2110.10111
[arXiv:2005.04195]: https://arxiv.org/pdf/2005.04195.pdf
[arXiv:2107.14180]: https://arxiv.org/pdf/2107.14180.pdf
